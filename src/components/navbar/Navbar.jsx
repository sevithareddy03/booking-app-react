import React from "react";
import { useNavigate } from "react-router-dom";
import "./Navbar.css";

const Navbar = () => {

  const navigate = useNavigate();

  const handleClick = () => {
    navigate("/");
  };
    return(
    <div className="navbar">
      <div className="navContainer">
        <span className="logo" onClick={handleClick}>Sevitha Reservations</span>
        <div className="navItems">
          <button className="navButton">Register</button>
          <button className="navButton">Login</button>
        </div>
      </div>
    </div>
    );
};

export default Navbar;